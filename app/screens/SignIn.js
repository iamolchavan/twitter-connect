import React from "react";
import { View } from "react-native";
import { Card, Button, FormLabel, FormInput } from "react-native-elements";
import { onSignIn } from "../auth";
export const _twitterSignIn = () => {
  RNTwitterSignIn.init(Constants.TWITTER_COMSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET)
  RNTwitterSignIn.logIn()
    .then(loginData => {
      console.log(loginData)  
      this.storeToken(loginData);      
      onSignIn().then(() => navigation.navigate("SignedIn"))                 
      const { authToken, authTokenSecret } = loginData
      if (authToken && authTokenSecret) {
        
      }
    })
    .catch(error => {
      console.log(error)
    }
  )
}
export default ({ navigation }) => (<View style={{ paddingVertical: 20 }}>
  <Card>        
    <Button
      buttonStyle={{ marginTop: 20 }}
      backgroundColor="#03A9F4"
      title="Connect With Twitter"
      onPress={_twitterSignIn}
    />
  </Card></View>
 
);
