import React from "react";
import { View } from "react-native";
import { Card, Button, FormLabel, FormInput } from "react-native-elements";
import { onSignIn } from "../auth";

export default class SignUp extends React.Component {
  // this.state = {
  //   name: '',
  //   email: '',
  //   password: '',
  //   error: null, // added this
  // };

  constructor(props) {
    super(props);

    this.state = {
    name: '',
    email: '',
    password: '',
    error: null, // added this
  };
  }

  isValid(){    
    const { email, password, name } = this.state;
    let valid = false;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;

    if (name.length > 0 && reg.test(email) === true && password.length > 0) {
      valid = true;
    }

    if (name.length === 0) {
      alert( 'You must enter a name');
    } else if(reg.test(email) === false){   
      alert( 'Email is Not Correct');
    }else if (email.length === 0) {
      alert( 'You must enter an email address');
    } else if (password.length === 0) {      
      alert( 'You must enter a password');
    }

    return valid;
  }

  onSignUp = () =>
  {
    const { email, password, name } = this.state;

    if (this.isValid()) {
       this.props.navigation.navigate("ConnectSocial");
    }
  }   
  
  render() {
  return(<View style={{ paddingVertical: 20 }}>
  <Card>
    <FormLabel>Name</FormLabel>
    <FormInput onChangeText={(name) => this.setState({name})} placeholder="name ..." />

    <FormLabel>Email</FormLabel>
    <FormInput 
    onChangeText={(email) => this.setState({email})}
    keyboardType="email-address"
    placeholder="Email address..." />
    <FormLabel>Password</FormLabel>
    <FormInput 
    onChangeText={(password) => this.setState({password})}
    secureTextEntry placeholder="Password..." />

    <Button
      buttonStyle={{ marginTop: 20 }}
      backgroundColor="#03A9F4"
      title="SIGN UP"
      onPress={this.onSignUp.bind(this)}
    />
  </Card></View>)}
};
