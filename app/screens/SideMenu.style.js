export default {
  container: {
    paddingTop: 20,
    flex: 1
  },
  navItemStyle: {
    padding: 20
  },
  navSectionStyle: {
    backgroundColor: 'lightgrey'
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  footerContainer: {
    padding: 10,
    backgroundColor: 'lightgrey'
  }
};
