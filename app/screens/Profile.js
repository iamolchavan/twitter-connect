import React from "react";
import {StyleSheet,AsyncStorage,ActivityIndicator,View} from 'react-native';
import { Card,  Text } from "react-native-elements";
import { CardItem,Thumbnail } from "native-base";
import { TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET} from "../auth";
import {create} from 'apisauce';
var OauthHelper = require('../service/oauth-helper.js');

export default class Profile extends React.Component {

//https://api.twitter.com/1.1/account/verify_credentials.json

  init(){
    AsyncStorage.getItem("twitterLogin").then((value) => {
      this.setState({twitterLogin: JSON.parse(value)});      
    });
  }
  constructor(props) {
    super(props);
    this.state = {
      twitterLogin:null,
      myjson: [],
      headerAuth:'',
      isLoading: true
    };
  }
  componentWillMount = () => {
    this.setState({
      isLoading: true
             
    });
    
  }
  componentDidMount = () => {
    AsyncStorage.getItem("twitterLogin").then((value) => {
      const obj = JSON.parse(value);
      this.setState({
        twitterLogin: obj               
      });
      
    }).then(res => {
      console.log("twitterLogin");
      console.log(this.state.twitterLogin);
      this.apiCall();       
  });
  }

  
  
  apiCall = () =>{
   const headerA = OauthHelper._buildRequestHeader(TWITTER_CONSUMER_KEY,this.state.twitterLogin.authTokenSecret,TWITTER_CONSUMER_SECRET,this.state.twitterLogin.authToken,'https://api.twitter.com/1.1/account/verify_credentials.json');
      console.log("headerA");
      console.log(headerA);

      const api = create({
        baseURL: 'https://api.twitter.com/1.1/',
        headers: {
          'Authorization': headerA,
          'Cache-Control': 'no-cache',
          'Host': 'api.twitter.com'
        }
      })
      .get('account/verify_credentials.json')
      .then((response) => {     
        console.log("===respone===") // yay!                
        if(response.status != 200){
          console.log("===error===") // yay!
          console.log(response.data.errors)      
          this.setState({
            myjson: [],
            isLoading: false 
          })
        }else{
          console.log(response)
          this.setState({
            myjson: response.data,
            isLoading: false 
          })
        }        
      });
  }

  render() {
    if(this.state.isLoading){
      return (<View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>)
    }
  const bgColor = '#'+this.state.myjson.profile_background_color;
   return( <View style={{ paddingVertical: 20 }}>
    <Card title={this.state.twitterLogin.userName} style={{backgroundColor: {bgColor}}}>
      <View
        style={{
          backgroundColor: "#bcbec1",
          alignItems: "center",
          justifyContent: "center",
          width: 80,
          height: 80,
          borderRadius: 40,
          alignSelf: "center",
          marginBottom: 20
        }}
      ><Thumbnail source={{uri:this.state.myjson.profile_image_url_https}} /></View>
        <CardItem>
        <Text>{this.state.myjson.name}</Text>
        </CardItem>
        <CardItem>
        <Text>{this.state.myjson.location}</Text>
        </CardItem>
        <CardItem>
        <Text style={{ color: "#bcbec1", alignItems: "center", }}>{this.state.twitterLogin.email}</Text>     
        </CardItem>      
        <CardItem>
        <Text style={{ color: "#bcbec1", alignItems: "center", }}>{this.state.myjson.description}</Text>
        </CardItem>      
    </Card>
  </View>
);
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})