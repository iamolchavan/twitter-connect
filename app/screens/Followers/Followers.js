
import React from 'react';
import { Text,StyleSheet,AsyncStorage,ActivityIndicator,View} from 'react-native';
import {Container, Content, Card, CardItem, Thumbnail, Left, Body} from 'native-base';

import { TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET} from "../../auth";
import {create} from 'apisauce';
var OauthHelper = require('../../service/oauth-helper.js');


class Followers extends React.Component {
  
  constructor(props) {
    super(props);
    AsyncStorage.getItem("twitterLogin").then((value) => {
      this.setState({twitterLogin: JSON.parse(value)});
    });
    this.state = {
      twitterLogin:null,
      myjson: [],
      headerAuth:'',
      isLoading: true
    };
  }
  componentDidMount = () => {
    AsyncStorage.getItem("twitterLogin").then((value) => {
      const obj = JSON.parse(value);
      this.setState({
        twitterLogin: obj,
      });
      
    }).then(res => {
      const headerA = OauthHelper._buildRequestHeader(TWITTER_CONSUMER_KEY,this.state.twitterLogin.authTokenSecret,TWITTER_CONSUMER_SECRET,this.state.twitterLogin.authToken,'https://api.twitter.com/1.1/followers/list.json',{user_id:this.state.twitterLogin.userID});
      const api = create({
        baseURL: 'https://api.twitter.com/1.1/',
        headers: {
          'Authorization':headerA,
          'Cache-Control': 'no-cache',
          'Host': 'api.twitter.com'
        }
      })
      .get('followers/list.json',{user_id:this.state.twitterLogin.userID})
      .then((response) => {
        if(response.status != 200){
          this.setState({
            myjson: [],
            isLoading: false 
          })
        }else{
          this.setState({
            myjson: response.data.users,
            isLoading: false 
          })
        }
      });      
  });
  }  
  
  render() {
    if(this.state.isLoading){
      return (<View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>)
    }
    if(this.state.myjson.length < 0){
      return (<View style={[styles.container, styles.horizontal]}>
       <Text style={{color:'rgba(0,0,0,0.3)'}} onPress={this.apiCall}>No Data Found</Text>
      </View>)
    }
    return (
      <Container>
        <Content>            
          {this.state.myjson.map((prop, key) => {
            return (
              <Card style={{flex: 0}} key={key}>
              <CardItem>
                <Left>
                  <Thumbnail source={{uri:prop.profile_image_url_https}} />
                  <Body>
                    <Text style={{fontWeight:'700'}}>
                      {prop.name} 
                    </Text>
                    <Text style={{color:'rgba(0,0,0,0.3)'}}>@{prop.screen_name}</Text>
                    <Text style={{color:'rgba(0,0,0,0.3)'}}>@{prop.description}</Text>
                  </Body>
                </Left>
              </CardItem>              
            </Card> 
            );
          })}               
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})
module.exports = Followers
