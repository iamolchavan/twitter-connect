import React, { Component } from 'react';
import {
  Text,
  View,
  NativeModules,AsyncStorage } from "react-native"
import { Card, Button } from "react-native-elements";
import { TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET} from "../auth";
import { createRootNavigatorTwitter } from '../router';
var OauthHelper = require('../service/oauth-helper.js')

const { RNTwitterSignIn } = NativeModules

class ConnectSocial extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
  }

  componentDidMount() {
      // const { checkedSignIn, signedIn } = this.state;      
  }
  _twitterSignIn = () => {
    RNTwitterSignIn.init(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
    RNTwitterSignIn.logIn()
      .then(loginData => {
        console.log(loginData)  
        AsyncStorage.setItem("twitterLogin", JSON.stringify(loginData));

        const { authToken, authTokenSecret } = loginData
        if (authToken && authTokenSecret) {
          AsyncStorage.setItem("twitterLogin", JSON.stringify(loginData));
          this.props.navigation.navigate("SignedIn");
        }
      })
      .catch(error => {
        console.log(error)
      }
    )
  }
 
  handleLogout = () => {
    console.log("logout")
    RNTwitterSignIn.logOut()
    this.setState({
      isLoggedIn: false
    })
  }

  render() {
    const { checkedSignIn, signedIn } = this.state;

    // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
    if (!checkedSignIn) {
      return (<View style={{ paddingVertical: 20 }}>
        <Card>       
          <Text>Connect Social Accounts</Text>
          <Text> Be in the with your favorite social media networks</Text> 
          <Button
            buttonStyle={{ marginTop: 20 }}
            backgroundColor="#03A9F4"
            title="Connect With Twitter"
            onPress={this._twitterSignIn}
          />
        </Card></View>
       
      );
    }

    const Layout = createRootNavigatorTwitter(signedIn);
    return <Layout />;        
  }
}

export default ConnectSocial;