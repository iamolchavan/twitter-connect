
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import styles from './SideMenu.style';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View, AsyncStorage} from 'react-native';
import { onSignOut,TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET} from "../auth";
var OauthHelper = require('../service/oauth-helper.js')

import Dialog from "react-native-dialog";
import { FormLabel, FormInput } from "react-native-elements";

class Home extends Component {  
  navigateToScreen = (route) => () => {    
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
   
  }
  constructor(props) {
    super(props);
    AsyncStorage.getItem("twitterLogin").then((value) => {
      const obj = JSON.parse(value);
      this.setState({
        twitterLogin: obj               
      });
      
    }).then(res => {});
    this.state = {
      loginUser: [],
      tweetsText:'',
      twitterLogin:null,
      dialogVisible: false
    };
  } 
  showDialog = () => {
    this.setState({ dialogVisible: true });
  };
 
  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };
 
  handlePost = () => {
    console.log("headerA");
    console.log(headerA);
    const obj = {
      status: this.state.tweetsText,
      user_id: this.state.twitterLogin.userID
    }
    const headerA = OauthHelper._buildRequestHeaderPOST(TWITTER_CONSUMER_KEY,this.state.twitterLogin.authTokenSecret,TWITTER_CONSUMER_SECRET,this.state.twitterLogin.authToken,'https://api.twitter.com/1.1/statuses/update.json',obj);
    
    var formBody = [];
    for (var property in obj) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(obj[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");  

    fetch('https://api.twitter.com/1.1/statuses/update.json', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Authorization': headerA,
        'Cache-Control': 'no-cache',
        'Host': 'api.twitter.com',        
      },
      body: formBody
    }).then((response) => {   
      console.log(response);
      this.setState({ dialogVisible: false });      
      if(response.error){

      }else{
        this.navigateToScreen('TweetsList');
      }
    });
    
  };
  render () {
    return (
      <View style={styles.container}>
        <ScrollView>          
          <View>
            <Text style={styles.sectionHeadingStyle}>
              {this.state.twitterLogin?this.state.twitterLogin.username:" "}
            </Text>
            <View style={styles.navSectionStyle}>
            <Text style={styles.navItemStyle} onPress={this.showDialog}>
                Post Tweets
            </Text>
            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('TweetsList')}>
                Tweets
              </Text>
            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('FavTweets')}>
                Favorites Tweets
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('MyTweets')}>
                My Tweets
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Followers')}>
                Follower
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Mentions_Timeline')}>
                Mentions Timeline
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Profile')}>
                Profile
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('DirectMessage')}>
                DirectMessage
              </Text>
              <Text style={styles.navItemStyle} onPress={() => onSignOut().then(() => this.props.navigation.navigate("SignedOut"))}>
                Logout
              </Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footerContainer}>
          <Text>Version 1.0</Text>
        </View>
        <Dialog.Container visible={this.state.dialogVisible}>
          <Dialog.Title>Enter your status</Dialog.Title>
          {/* <Dialog.Description>
          
          </Dialog.Description> */}
          <FormLabel>tweets</FormLabel>
          <FormInput onChangeText={(tweetsText) => this.setState({tweetsText})} placeholder="tweets ..." />

          <Dialog.Button label="Cancel" onPress={this.handleCancel}/>
          <Dialog.Button label="Post" onPress={this.handlePost}/>
        </Dialog.Container>
      </View>
    );
  }
}

Home.propTypes = {
  navigation: PropTypes.object
};

export default Home;