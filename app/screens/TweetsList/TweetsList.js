import React from 'react';
import {Image, Text,StyleSheet,AsyncStorage,ActivityIndicator,View} from 'react-native';
import {Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';
var OauthHelper = require('../../service/oauth-helper.js')
import LinkPreview from 'react-native-link-preview';
import Autolink from 'react-native-autolink'; // 1.1.0
import { TWITTER_CONSUMER_KEY,TWITTER_CONSUMER_SECRET } from "../../auth";
import {create} from 'apisauce'

class TweetsList extends React.Component {
  init(){
    AsyncStorage.getItem("twitterLogin").then((value) => {
      this.setState({twitterLogin: JSON.parse(value)});      
    });
  }
  constructor(props) {
    super(props);
    this.state = {
      twitterLogin:null,
      myjson: [],
      headerAuth:'',
      isLoading: true
    };
  }
  componentWillMount = () => {
    this.setState({
      isLoading: true
             
    });
    
  }
  componentDidMount = () => {
    AsyncStorage.getItem("twitterLogin").then((value) => {
      const obj = JSON.parse(value);
      this.setState({
        twitterLogin: obj               
      });
      
    }).then(res => {
      this.apiCall();      
  });
  }  
  
  apiCall = () =>{
   const headerA = OauthHelper._buildRequestHeader(TWITTER_CONSUMER_KEY,this.state.twitterLogin.authTokenSecret,TWITTER_CONSUMER_SECRET,this.state.twitterLogin.authToken,'https://api.twitter.com/1.1/statuses/home_timeline.json');
      console.log("headerA");
      console.log(headerA);

      const api = create({
        baseURL: 'https://api.twitter.com/1.1/',
        headers: {
          'Authorization': headerA,
          'Cache-Control': 'no-cache',
          'Host': 'api.twitter.com'
        }
      })
      .get('statuses/home_timeline.json')
      .then((response) => {     
        console.log("===respone===") // yay!                
        if(response.status != 200){
          console.log("===error===") // yay!
          console.log(response.data.errors)      
          this.setState({
            myjson: [],
            isLoading: false 
          })
        }else{
          console.log(response)
          this.setState({
            myjson: response.data,
            isLoading: false 
          })
        }        
      });
  }

 

  render() {
    if(this.state.isLoading){
      return (<View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>)
    }
    if(this.state.myjson.length < 0){
      return (<View style={[styles.container, styles.horizontal]}>
       <Text style={{color:'rgba(0,0,0,0.3)'}} onPress={this.apiCall}>No Data Found</Text>
      </View>)
    }
    return (
      <Container>
        <Content>            
          { this.state.myjson.map((prop, key) => {
            return (
              <Card style={{flex: 0}} key={key}>
              <CardItem>
                <Left>
                  <Thumbnail source={{uri:prop.user.profile_image_url_https}} />
                  <Body>
                    <Text style={{fontWeight:'700'}}>
                      {prop.user.name} 
                      <Text style={{color:'rgba(0,0,0,0.3)'}}>@{prop.user.screen_name}</Text>
                    </Text>
                    <Text note style={{fontWeight:'100'}}>{prop.created_at}</Text>
                  </Body>
                </Left>
              </CardItem>
              <CardItem>
                <Body>
                <Text>
                <Autolink
          text={prop.text}
          hashtag="instagram"
          mention="twitter"
        />
                </Text>
                {prop.extended_entities?prop.extended_entities.media.map((val,k)=>{
                        return(<Thumbnail key={k} source={{uri: val.media_url_https}}  resizeMode={'cover'}
                        style={{ width: '100%', height: 200 }}/>);
                      }):<Text></Text>}
                </Body>
              </CardItem>
              <CardItem>
                <Left>
                  <Grid>
                    <Col size={4}></Col>
                    <Col size={24} style={{flexDirection:'row'}}>
                      {/* <Icon name="ios-text-outline"/> */}
                      <Text style={{color:'rgba(0,0,0,0.3)', fontSize:16}}>rt:
                      <Text style={{color:'rgba(0,0,0,0.3)', fontSize:16}}> {prop.retweet_count}</Text>
                      </Text>
                    </Col>
                    <Col size={24} style={{flexDirection:'row'}}>
                      {/* <Icon name="ios-repeat"/> */}<Text style={{color:'rgba(0,0,0,0.3)', fontSize:16}}>fav:
                      <Text style={{color:'rgba(0,0,0,0.3)', fontSize:16}}> {prop.favorite_count}</Text>                     
                      </Text>

                    </Col>                    
                  </Grid>
                </Left>
              </CardItem>
            </Card> 
            );
          })}               
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})
module.exports = TweetsList