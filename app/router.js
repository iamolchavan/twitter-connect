import React from "react";
import { Platform, StatusBar,Text } from "react-native";
import {
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";

import SignUp from "./screens/SignUp";
import ConnectSocial from "./screens/ConnectSocial";
import Home from "./screens/Home";
import Profile from "./screens/Profile";

import TweetsList from './screens/TweetsList/TweetsList';
import Mentions_Timeline from './screens/TweetsList/Mentions_Timeline';
import MyTweets from './screens/TweetsList/MyTweets';
import FavTweets from './screens/TweetsList/FavTweets';

import Followers from './screens/Followers/Followers';
import DirectMessage from './screens/DirectMessage/DirectMessage';
import {createDrawerNavigator} from 'react-navigation';
const headerStyle = {
  marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
};

export const SignedOut = createStackNavigator({
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      title: "Sign Up",
      headerStyle
    }
  },
  ConnectSocial: {
    screen: ConnectSocial,
    navigationOptions: {
      title: "Connect Social",
      headerStyle
    }
  },Home: {
    screen: Home,
    navigationOptions: {
      title: "Home",
      headerStyle,
      headerLeft: <Text name="menu" size={35} onPress={ () => navigation.toggleDrawer() }>menu</Text>
    }
  }
});


export const SignedIn = createDrawerNavigator({  
  TweetsList: {
    screen: TweetsList,
    navigationOptions: {
      title: 'Timeline' 
    }
  },
  Followers: {
    screen: Followers,
    navigationOptions: {
      title: 'Followers' 
    }
  },
  Mentions_Timeline: {
    screen: Mentions_Timeline,
    navigationOptions: {
      title: 'MentionTimeline' 
    }
  },
  MyTweets: {
    screen: MyTweets,
    navigationOptions: {
      title: 'My Tweets' 
    }
  },
  FavTweets: {
    screen: FavTweets,
    navigationOptions: {
      title: 'Favoirates' 
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      title: 'Profile' 
    }
  },
  DirectMessage: {
    screen: DirectMessage,
    navigationOptions: {
      title: 'DirectMessage' 
    }
  }
}, {
  contentComponent: Home,
  drawerWidth: 200  
});

export const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      SignedIn: {
        screen: SignedIn
      },
      SignedOut: {
        screen: SignedOut
      }
    },
    {
      initialRouteName: signedIn ? "SignedIn" : "SignedOut"
    }
  );
};
