var ohauth = require('ohauth');

class TOhauth {

const config = {
    twitter: {
      consumer_key: '*',
      consumer_secret: '*',
    },
    facebook: {
      client_id: '*',
      client_secret: '*'
    },
  }

  // Create the manager
const manager = new OAuthManager('Connect')
// configure the manager
manager.configure(config);

module.exports = {

  authenticate(providerName) {
    return new Promise((resolve, reject) => {
      let promise; 
      switch (providerName) {
        case 'twitter':
          promise = manager.authorize('twitter');
          break;
        case 'facebook':
          promise = manager.authorize('facebook', {scopes: 'user_posts'});        
          break;         
        default:
          console.log('Unknown provider');
          break;
      }
      if(promise){
        promise.then((res) => {
          resolve({ credentials: res.response.credentials, uuid: res.response.uuid });
        }).catch((error) =>{
          reject(error);
        });
      }else{
        reject('unknown provider');
      }
    });
  }

}

generateTwitterOauthHeader(method, url, urlParams, token, tokenSecret){    

    let headerParams = {
      oauth_consumer_key: config.twitter.consumer_key,
      oauth_nonce: this._getNonce(),
      oauth_signature_method: "HMAC-SHA1",
      oauth_timestamp: ohauth.timestamp(),
      oauth_token: token,
      oauth_version: "1.0"
    };    

    headerParams = Object.assign(headerParams, urlParams);
    console.log(headerParams);

    let baseString = ohauth.baseString(method, url, headerParams)
    console.log(baseString);

    let sig = ohauth.signature(config.twitter.consumer_secret, tokenSecret, baseString) // function(oauth_secret, token_secret, baseString) {
    console.log(sig)

    headerParams["oauth_signature"] = sig;
    let header = ohauth.authHeader(headerParams);
    console.log(header);    

    return header
  }

    _getNonce(){
    for (var o = ''; o.length < 32;) {
      o += '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'[Math.floor(Math.random() * 61)];
    }
    return o;    
  }
}

module.exports = OauthHelper