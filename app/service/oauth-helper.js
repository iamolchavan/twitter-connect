// @flow

import hmacsha1 from 'hmacsha1'
import CryptoJS from 'crypto-js'
import PercentEncoder from './percent-encoder.js'
import oauthSignature from 'oauth-signature';
import { TWITTER_CONSUMER_SECRET, TWITTER_ACCESS_TOKEN_KEY_SECRET } from '../auth.js';
var options = {
  encodeSignature: true // will encode the signature following the RFC 3986 Spec by default
};
class OauthHelper {
  consumerSecret: string;
   
  constructor (consumerSecret: string) {
    this.consumerSecret = consumerSecret
  }

  buildAuthorizationHeader (method: string, url: string, params: any, queryParams: any, oauthTokenSecret: string) {
    let output = 'OAuth '
    let signature = this.getSigningKey(method, url, params, queryParams, oauthTokenSecret)
    for (let key in params) {
      output += PercentEncoder.encode(key) + '="' + PercentEncoder.encode(params[key]) + '", '
    }
    output += 'oauth_signature="' + PercentEncoder.encode(signature) + '"'
    return output
  }

 static getSigningKey (method: string, url: string, params: any, queryParams: any, oauthTokenSecret: string) {
    let signingKey = PercentEncoder.encode(this.consumerSecret) + '&' + PercentEncoder.encode(oauthTokenSecret)
    let signatureBase = OauthHelper._getSignatureBase(method, url, params, queryParams)
    return CryptoJS.enc.Base64.stringify(HmacSHA1(signatureBase, signingKey))
  }

  // Returns an object with oauth_token and oauth_verifier
  static getOauthTokenAndVerifierFromURLCallback (urlCallback: string) {
    let prefix = 'react-twitter-oauth://callback?'
    let content = urlCallback.substr(prefix.length)

    let result = {}
    content.split('&').forEach((pair) => {
      let values = pair.split('=')
      result[values[0]] = values[1]
    })

    return result
  }

  static _getSignatureBase (method: string, url: string, params: any, queryParams: any) {
    return method.toUpperCase() + '&' + PercentEncoder.encode(url) + '&' +
      PercentEncoder.encode(OauthHelper._collectParameters(params, queryParams))
  }

  static _collectParameters (params: any, queryParams: any) {
    let encodedParams = OauthHelper._percentEncodeParams(params, queryParams)
    OauthHelper._sortEncodedParams(encodedParams)
    return OauthHelper._joinParams(encodedParams)
  }

  static _percentEncodeParams (params: any, queryParams: any) {
    let encodedParams = []
    for (let key in params) {
      encodedParams.push({
        key: `${PercentEncoder.encode(key)}`,
        value: `${PercentEncoder.encode(params[key])}`
      })
    }
    for (let key in queryParams) {
      encodedParams.push({
        key: `${PercentEncoder.encode(key)}`,
        value: `${PercentEncoder.encode(queryParams[key])}`
      })
    }
    return encodedParams
  }

  static _sortEncodedParams (params: any) {
    params.sort((first, second) => {
      return (first.key > second.key) ? 1 : ((second.key > first.key) ? -1 : 0)
    })
  }

  static _joinParams (params: any) {
    let output = ''
    params.forEach((param) => {
      if (output.length > 0) {
        output += '&'
      }
      output += param.key + '=' + param.value
    })
    return output
  }

  static generateNonce () {
    var text = ''
    var length = 6
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for (var i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return text
  }

static  _buildRequestHeaderPOST(user_auth_token, accesstoken_secret,user_auth_token_secret,accesstoken,url,params){
    var val = Date.now() / 1000;      
    var dateString = parseInt(val);
    var generateNonceString = this.generateNonce();
    var httpMethod = 'POST';    
    var parameters = {
        oauth_consumer_key : user_auth_token,
        oauth_token : accesstoken,
        oauth_nonce : generateNonceString,
        oauth_timestamp : dateString,
        oauth_signature_method : 'HMAC-SHA1',
        oauth_version : '1.0',        
    };
    for (var key in params) {
      parameters[key] = params[key];
    }
    console.log(parameters);
    console.log(user_auth_token_secret);
    console.log(accesstoken_secret);
    console.log(url);
    var formBody = [];
    for (var property in parameters) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(parameters[property]);
      formBody.push(encodedKey + "=" +'"'+encodedValue+'"');
    }
    var signature = oauthSignature.generate(httpMethod, url, parameters, user_auth_token_secret, accesstoken_secret, {encodeSignature: true});          
    formBody.push(encodeURIComponent("oauth_signature") +'='+'"'+signature+'"');
    formBody = formBody.join(","); 
    
    return 'OAuth '+formBody;
  }
static  _buildRequestHeader(user_auth_token, accesstoken_secret,user_auth_token_secret,accesstoken,url,params){
    var val = Date.now() / 1000;      
    var dateString = parseInt(val);
    var generateNonceString = this.generateNonce();
    var httpMethod = 'GET';    
    var parameters = {
        oauth_consumer_key : user_auth_token,
        oauth_token : accesstoken,
        oauth_nonce : generateNonceString,
        oauth_timestamp : dateString,
        oauth_signature_method : 'HMAC-SHA1',
        oauth_version : '1.0',        
    };
    for (var key in params) {
      parameters[key] = params[key];
    }
    console.log(parameters);
    console.log(user_auth_token_secret);
    console.log(accesstoken_secret);
    console.log(url);
    var formBody = [];
    for (var property in parameters) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(parameters[property]);
      formBody.push(encodedKey + "=" +'"'+encodedValue+'"');
    }
    var signature = oauthSignature.generate(httpMethod, url, parameters, user_auth_token_secret, accesstoken_secret, {encodeSignature: true});          
    formBody.push(encodeURIComponent("oauth_signature") +'='+'"'+signature+'"');
    formBody = formBody.join(","); 
    
    return 'OAuth '+formBody;
  }
}

module.exports = OauthHelper
